import React from "react";
import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";
import styles from "./Header.module.scss";

const Header = () => {
  const { pathname } = useLocation();
  return (
    <div className={styles.header}>
      <h2 className={styles.logo}>
        <Link to="/">Users Panel</Link>
      </h2>
      <p className={pathname === "/" ? styles.activeItem : null}>
        <Link to="/">Users list</Link>
      </p>
    </div>
  );
};

export default Header;

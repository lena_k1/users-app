import React from "react";
import { Link } from "react-router-dom";
import styles from "./User.module.scss";

const User = ({ user, onClick }) => {
  const {
    id,
    first_name,
    last_name,
    birth_date,
    gender,
    job,
    biography,
    is_active,
  } = user;

  return (
    <div className={styles.user}>
      <h2>
        {first_name} {last_name}
      </h2>
      <p>
        <span>Birt date:</span> {birth_date}
      </p>
      <p>
        <span>Gender:</span> {gender}
      </p>
      <p>
        <span>Job:</span> {job}
      </p>
      <p>
        <span>Biography:</span> {biography}
      </p>
      <p style={{ color: is_active ? "green" : "red" }}>
        User {is_active ? "Active" : "Inactive"}
      </p>
      <div className={styles.btns}>
        <Link to={`/user?id=${id}`}>
          <button className={styles.editBtn}>Edit</button>
        </Link>

        <button className={styles.deleteBtn} onClick={() => onClick(id)}>
          Delete
        </button>
      </div>
    </div>
  );
};

export default User;

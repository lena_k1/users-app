import React from "react";
import { fields } from "../../config/constants";
import styles from "./Form.module.scss";

const Form = ({ onChange, onSubmit, values, isAddPage, errors }) => {
  return (
    <>
      <h2 className={styles.title}> {isAddPage ? "Add" : "Edit"} User</h2>
      <form onSubmit={onSubmit} className={styles.form}>
        {fields.map(({ name, type, placeholder, required, maxLength, options }) => (
          <div className={styles.formGroup} key={name}>
            {type === "text" || type === "date" || type === "checkbox" ? (
              <>
                <label htmlFor={name} className={styles.label}>
                  {placeholder}
                  <span>*</span>
                </label>
                <input
                  id={name}
                  className={styles.input}
                  onChange={onChange}
                  name={name}
                  type={type}
                  min={0}
                  placeholder={placeholder}
                  required={required}
                  value={values[name]}
                  maxLength={maxLength}
                  checked={values[name]}
                />
              </>
            ) : (
              <>
                {type === "textarea" ? (
                  <>
                    <label htmlFor={name}>{placeholder} </label>
                    <textarea
                      id={name}
                      className={styles.input}
                      onChange={onChange}
                      name={name}
                      type={type}
                      placeholder={placeholder}
                      required={required}
                      value={values[name]}
                      maxLength={maxLength}
                    />
                  </>
                ) : (
                  <>
                    <label htmlFor={name}>{placeholder} </label>
                    <select name={name} id={name} onChange={onChange}>
                      {options.map((option) => (
                        <option key={option} defaultValue={values[name] === option} value={option}>
                          {option}
                        </option>
                      ))}
                    </select>
                  </>
                )}
              </>
            )}
            {errors[name] && <p className={styles.error}>{errors[name]}</p>}
          </div>
        ))}

        <button type="submit" className={styles.btn}>
          Submit
        </button>
      </form>
    </>
  );
};

export default Form;

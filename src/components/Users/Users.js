import React from "react";
import { Link } from "react-router-dom";
import styles from "./Users.module.scss";

const Users = ({ users, onClick }) => {
  return (
    <div className={styles.users}>
      {users.map(({ id, last_name, first_name, birth_date, gender }) => {
        return (
          <div className={styles.item} key={id}>
            <p className={styles.name}>
              <Link to={`/${id}`}>
                {first_name} {last_name}{" "}
              </Link>
            </p>
            <p>
              <span>Birt date:</span> {birth_date}
            </p>
            <p>
              <span>Gender:</span> {gender}
            </p>
            <button className={styles.deleteBtn} onClick={() => onClick(id)}>
              Delete
            </button>
          </div>
        );
      })}
      
    </div>
  );
};

export default Users;

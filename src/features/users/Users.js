import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getUsersFetch, selectUsers } from "../../redux/ducks/usersSlice";
import { deleteUserRequest } from "../../redux/ducks/deleteSplice";
import { resetUser } from "../../redux/ducks/userSlice";
import styles from "../../components/Users/Users.module.scss";
import UsersComponent from "../../components/Users/Users";

const Users = () => {
  const dispatch = useDispatch();
  const users = useSelector(selectUsers);

  useEffect(() => {
    dispatch(getUsersFetch());
    dispatch(resetUser());
  }, []);

  const handleDeleteClick = (id) => {
    dispatch(deleteUserRequest(id));
  };

  return (
    <>
      {users?.length !== 0 ? (
        <UsersComponent users={users} onClick={handleDeleteClick} />
      ) : (
        <div className={styles.notFound}>Users not found( Add new!</div>
      )}
      <Link to="/user">
        <button className={styles.addBtn}>+</button>
      </Link>
    </>
  );
};

export default Users;

import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  getUserFetch,
  selectUser,
  selectStatus,
} from "../../redux/ducks/userSlice";
import { deleteUserRequest } from "../../redux/ducks/deleteSplice";
import UserComponent from "../../components/User/User";

const User = () => {
  const dispatch = useDispatch();
  const user = useSelector(selectUser);
  const status = useSelector(selectStatus);

  const { userId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getUserFetch(userId));
  }, []);

  useEffect(() => {
    status === "failed" && navigate("/");
  }, [status]);

  const handleClick = (id) => {
    dispatch(deleteUserRequest(id));
    setTimeout(() => {
      navigate("/");
    }, 1000);
  };

  return (
    <>
      <UserComponent user={user} onClick={handleClick} />
    </>
  );
};

export default User;

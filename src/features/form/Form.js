import React, { useEffect } from "react";
import { useSearchParams, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  getUserFetch,
  selectUser,
  updateUserFields,
  selectStatus,
  updateUserRequest,
  addUserRequest,
  selectValidationErrors,
} from "../../redux/ducks/userSlice";
import Form from "../../components/Form/Form";

const Users = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [searchParam, setSearchParam] = useSearchParams();
  const user = useSelector(selectUser);
  const status = useSelector(selectStatus);
  const validationErrors = useSelector(selectValidationErrors);

  const isAddPage = Boolean(!user?.id);

  useEffect(() => {
    if (searchParam.get("id") && !user?.id) {
      dispatch(getUserFetch(searchParam.get("id")));
    }
  }, []);

  useEffect(() => {
    status === "failed" && navigate("/");
  }, [status]);

  const handleChange = (e) => {
    const { target } = e;
    if (target.type === "checkbox") {
      return dispatch(updateUserFields({ [target.name]: Boolean(target.checked) }));
    }
    dispatch(updateUserFields({ [target.name]: target.value }));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (isAddPage) {
      return dispatch(addUserRequest(user));
    }
    dispatch(updateUserRequest({ id: user.id, data: user }));
  };

  return (
    <>
      <Form
        values={user}
        onChange={handleChange}
        onSubmit={handleSubmit}
        errors={validationErrors}
        isAddPage={isAddPage}
      />
    </>
  );
};

export default Users;

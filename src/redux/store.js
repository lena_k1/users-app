import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";

import usersSlice from "../redux/ducks/usersSlice";
import userSlice from "../redux/ducks/userSlice";
import deleteUserSlice from "./ducks/deleteSplice";

import { watcherSaga } from "./sagas/rootSaga";
const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: {
    users: usersSlice,
    user: userSlice,
    deleting: deleteUserSlice,
  },
  middleware: [sagaMiddleware],
});

sagaMiddleware.run(watcherSaga);

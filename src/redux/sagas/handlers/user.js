import { call, put } from "redux-saga/effects";
import { getUserSuccess, getUserFailure } from "../../ducks/userSlice";
import { requestGetUser } from "../requests/user";
import { toast } from "react-toastify";

export function* handleGetUser(action) {
  try {
    const response = yield call(requestGetUser, action.payload);
    const { data } = response;
    yield put(getUserSuccess(data));
  } catch (error) {
    yield put(getUserFailure(error.message));
    toast.error("Failed to get user");
  }
}

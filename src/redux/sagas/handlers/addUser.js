import { call, put } from "redux-saga/effects";
import { addUserSuccess, addUserFailure, setValidationErrors, resetValidationErrors } from "../../ducks/userSlice";
import { requestAddUser } from "../requests/addUser";
import validateForm from "../../../utils/validation/formValidation";
import { toast } from "react-toastify";

export function* handleAddUser(action) {
  try {
    const validation = validateForm(action.payload);

    if (Object.keys(validation).length === 0) {
      const response = yield call(requestAddUser, action.payload);
      const { data } = response;
      yield put(addUserSuccess(data));
      yield put(resetValidationErrors());
      toast.success("User added successfully");
    } else {
      yield put(setValidationErrors(validation));
    }
  } catch (error) {
    const errInfo = { action: addUserFailure.type, message: error.message };
    yield put(addUserFailure(errInfo));
    toast.error("Failed to add user");
  }
}

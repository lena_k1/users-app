import { call, put } from "redux-saga/effects";
import { getUsersFetch } from "../../ducks/usersSlice";
import {
  deleteUserSuccess,
  deleteUserFailure,
  resetDeleteState,
} from "../../ducks/deleteSplice";
import { requestDeleteUser } from "../requests/deleteUser";
import { toast } from "react-toastify";

export function* handleDeleteUser(action) {
  try {
    yield call(requestDeleteUser, action.payload);
    yield put(deleteUserSuccess());
    yield put(resetDeleteState());
    toast.success("User deleted successfully");
    yield put(getUsersFetch());
  } catch (error) {
    yield put(deleteUserFailure(error.message));
    toast.error("Failed to delete user");
  }
}

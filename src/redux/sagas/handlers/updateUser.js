import { call, put } from "redux-saga/effects";
import {
  updateUserSuccess,
  updateUserFailure,
  setValidationErrors,
  resetValidationErrors,
} from "../../ducks/userSlice";
import { requestUpdateUser } from "../requests/updateUser";
import validateForm from "../../../utils/validation/formValidation";
import { toast } from "react-toastify";

export function* handleUpdateUser(action) {
  try {
    const validation = validateForm(action.payload.data);

    if (Object.keys(validation).length === 0) {
      const response = yield call(requestUpdateUser, action.payload);
      const { data } = response;
      yield put(updateUserSuccess(data));
      yield put(resetValidationErrors());
      toast.success("User updated successfully");
    } else {
      yield put(setValidationErrors(validation));
    }
  } catch (error) {
    const errInfo = { action: updateUserFailure.type, message: error.message };
    yield put(updateUserFailure(errInfo));
    toast.error("Failed to update user");
  }
}

import { call, put } from "redux-saga/effects";
import { getUsersSuccess, getUsersFailure } from "../../ducks/usersSlice";
import { requestGetUsers } from "../requests/users";
import { toast } from "react-toastify";

export function* handleGetUsers() {
  try {
    const response = yield call(requestGetUsers);
    const { data } = response;
    yield put(getUsersSuccess(data));
  } catch (error) {
    yield put(getUsersFailure(error.message));
    toast.error("Failed to get users");
  }
}

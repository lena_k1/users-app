import api from "../../../services/api";

export function requestUpdateUser({id, data}) {
  return api.put(`/contact/${id}`, data);
}

import api from "../../../services/api";

export function requestAddUser(data) {
  return api.post(`/contact/`, data);
}

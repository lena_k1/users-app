import api from "../../../services/api";

export function requestGetUser(id) {
  return api.get(`/contact/${id}/`);
}

import api from "../../../services/api";

export function requestDeleteUser(id) {
  return api.delete(`/contact/${id}/`);
}

import { takeLatest, takeEvery } from "redux-saga/effects";
//handlers
import { handleGetUsers } from "./handlers/users";
import { handleDeleteUser } from "./handlers/deleteUser";
import { handleGetUser } from "./handlers/user";
import { handleUpdateUser } from "./handlers/updateUser";
import { handleAddUser } from "./handlers/addUser";
//actions
import { getUsersFetch } from "../ducks/usersSlice";
import { deleteUserRequest } from "../ducks/deleteSplice";
import {
  getUserFetch,
  updateUserRequest,
  addUserRequest,
} from "../ducks/userSlice";

export function* watcherSaga() {
  yield takeLatest(getUsersFetch.type, handleGetUsers);
  yield takeEvery(deleteUserRequest.type, handleDeleteUser);
  yield takeEvery(getUserFetch.type, handleGetUser);
  yield takeEvery(updateUserRequest.type, handleUpdateUser);
  yield takeEvery(addUserRequest.type, handleAddUser);
}

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  status: "idle",
  error: null,
};

export const deleteUserSlice = createSlice({
  name: "deleteUser",
  initialState,
  reducers: {
    deleteUserRequest: (state) => {
      state.status = "loading";
    },
    deleteUserSuccess: (state) => {
      state.status = "succeeded";
    },
    deleteUserFailure: (state, action) => {
      state.error = action.payload;
      state.status = "failed";
    },
    resetDeleteState: () => initialState,
  },
});

export const {
  deleteUserRequest,
  deleteUserSuccess,
  deleteUserFailure,
  resetDeleteState,
} = deleteUserSlice.actions;

export const selectDeleteStatus = (state) => state.deleting.status;

export default deleteUserSlice.reducer;

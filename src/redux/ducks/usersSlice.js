import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: [],
  status: "idle",
  error: null,
};

export const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {
    getUsersFetch: (state) => {
      state.status = "loading";
    },
    getUsersSuccess: (state, action) => {
      state.items = action.payload;
      state.status = "succeeded";
    },
    getUsersFailure: (state, action) => {
      state.status = "failed";
      state.error = action.payload;
    },
  },
});

export const { getUsersFetch, getUsersSuccess, getUsersFailure } =
  usersSlice.actions;

export const selectUsers = (state) => state.users.items;
export const selectStatus = (state) => state.users.status;

export default usersSlice.reducer;

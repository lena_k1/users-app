import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  item: {
    first_name: "",
    last_name: "",
    birth_date: "",
    gender: "male",
    job: "",
    biography: "",
    is_active: true,
  },
  validationErrors: {},
  status: "idle",
  error: null,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    //fetching
    getUserFetch: (state) => {
      state.status = "loading";
    },
    getUserSuccess: (state, action) => {
      state.item = action.payload;
      state.status = "succeeded";
    },
    getUserFailure: (state, action) => {
      state.status = "failed";
      state.error = action.payload;
    },

    // updating
    updateUserRequest: (state) => {
      state.status = "loading";
    },
    updateUserSuccess: (state) => {
      state.status = "succeeded";
    },
    updateUserFailure: (state, action) => {
      state.status = "failed";
      state.error = action.payload;
    },

    // adding
    addUserRequest: (state) => {
      state.status = "loading";
    },
    addUserSuccess: (state) => {
      state.status = "succeeded";
      state.item = initialState.item;
    },
    addUserFailure: (state, action) => {
      state.status = "failed";
      state.error = action.payload;
    },

    updateUserFields: (state, action) => {
      state.item = { ...state.item, ...action.payload };
    },
    setValidationErrors: (state, action) => {
      state.validationErrors = action.payload;
    },
    resetUser: () => initialState,
    resetValidationErrors: (state) => {
      state.validationErrors = {};
    },
  },
});

export const {
  getUserFetch,
  getUserSuccess,
  getUserFailure,

  updateUserRequest,
  updateUserSuccess,
  updateUserFailure,

  addUserRequest,
  addUserSuccess,
  addUserFailure,

  resetUser,
  updateUserFields,
  setValidationErrors,
  resetValidationErrors,
} = userSlice.actions;

export const selectUser = (state) => state.user.item;
export const selectStatus = (state) => state.user.status;
export const selectValidationErrors = (state) => state.user.validationErrors;

export default userSlice.reducer;

import axios from "axios";
const API_URL = "https://frontend-candidate.dev.sdh.com.ua/v1";

const api = axios.create({
  baseURL: API_URL,
});

export default api;


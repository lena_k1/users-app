import { fields } from "../../config/constants";

const validateField = (field, value, maxLength) => {
  let error = {};

  if (field === "is_active") return;
  if (!value) {
    error[field] = "The field is required";
  }
  if (maxLength) {
    if (value.length > maxLength) {
      error[field] = `The number of characters must be less than ${maxLength}`;
    }
  }

  return error;
};

const validateFields = (values) => {
  let errors = {};

  for (let key in values) {
    const maxLength = fields.find((field) => field.name === key)?.maxLength;
    errors = { ...errors, ...validateField(key, values[key], maxLength) };
  }

  return errors;
};

export default validateFields;

export const fields = [
  {
    name: "first_name",
    placeholder: "First Name",
    type: "text",
    required: true,
    maxLength: 256,
  },
  {
    name: "last_name",
    placeholder: "Last Name",
    type: "text",
    required: true,
    maxLength: 256,
  },
  {
    name: "birth_date",
    placeholder: "Birth Date",
    type: "date",
    required: true,
  },
  {
    name: "gender",
    placeholder: "Gender",
    type: "select",
    required: true,
    options: ["male", "female"],
  },
  {
    name: "job",
    placeholder: "Job",
    type: "text",
    required: true,
    maxLength: 256,
  },
  {
    name: "biography",
    placeholder: "Biography",
    type: "textarea",
    required: true,
    maxLength: 1024,
  },
  {
    name: "is_active",
    placeholder: "User Active",
    type: "checkbox",
  },
];

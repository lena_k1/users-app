import React from "react";
import styles from "./App.module.scss";
import { BrowserRouter as Router, Route, Routes, Navigate } from "react-router-dom";
import Header from "./components/Header/Header";
import Users from "./features/users/Users";
import UserInfo from "./features/user/User";
import UserForm from "./features/form/Form";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <Router>
      <div className={styles.app}>
        <Header />
        <Routes>
          <Route exact path="/" element={<Users />} />
          <Route exact path="/:userId" element={<UserInfo />} />
          <Route exact path="/user" element={<UserForm />} />
          <Route path="*" element={<Navigate to="/" />} />
        </Routes>
      </div>
      <ToastContainer autoClose={1000} />
    </Router>
  );
}

export default App;
